# LetMeKnow

LetMeKnow is an Android app that notifies you of new releases of selected open-source apps.

Why?
- You don't have the Google Play Store.
- You can't find the app on F-Droid.

It detects changes in the selected repositories and offers you to download the latest APK, sparing you the trouble of visiting repositories manually to check on updates. 

## Local Setup

### Prerequisites

- Node and NPM
- Expo CLI

## Development

### Run App
- `expo start`

## Screenshots
<img src="./assets/letmeknow_homeScreen.png" width="200">
<img src="./assets/letmeknow_modal.png" width="200">