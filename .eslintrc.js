module.exports = {
  env: {
    'react-native/react-native': true,
  },
  extends: [
    'airbnb',
    'plugin:import/typescript',
    'plugin:react/recommended',
    'plugin:react-native/all',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'react-native',
    '@typescript-eslint',
  ],
  rules: {
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        ts: 'never',
        tsx: 'never',
      },
    ],
    'react/jsx-filename-extension': [1, { extensions: ['.tsx', '.ts'] }],
    'react/require-default-props': [0],
    'no-use-before-define': [0],
    indent: [2, 2],
  },
};
