import AsyncStorage from '@react-native-async-storage/async-storage';
import { scheduleNotificationAsync } from 'expo-notifications';
import { fetchFirefox, fetchProtonMail, fetchSignal } from './api';
import { Version } from './types';

export default async function refresh(): Promise<Record<string, Version>> {
  const versionsArray = await Promise.all([
    fetchProtonMail(),
    fetchFirefox(),
    fetchSignal(),
  ]);

  const versions = {
    protonMail: versionsArray[0],
    firefox: versionsArray[1],
    signal: versionsArray[2],
  };

  const oldProtonMail = await getData('protonMail');
  const oldFirefox = await getData('firefox');
  const oldSignal = await getData('signal');

  compareDataAndTriggerActions(oldProtonMail, versions.protonMail.version, 'protonMail');
  compareDataAndTriggerActions(oldFirefox, versions.firefox.version, 'firefox');
  compareDataAndTriggerActions(oldSignal, versions.signal.version, 'signal');

  return versions;
}

async function storeData(key: string, version: string) {
  await AsyncStorage.setItem(key, version);
  console.log('storeData key:', key);
  console.log('storeData value:', version);
}

async function getData(key : string): Promise<string | null> {
  return AsyncStorage.getItem(key);
}

async function sendNotification(key: string) {
  await scheduleNotificationAsync({
    content: {
      title: 'Letmeknow has an update! 📬',
      body: `New version of ${key} is available!`,
    },
    trigger: null,
  });
}

function compareDataAndTriggerActions(oldApp: string | null, newApp: string, key: string) {
  if (oldApp !== newApp) {
    sendNotification(key);
    storeData(key, newApp);
  } else {
    console.log(`no update of ${key}`);
  }
}
