import { NavigationScreenProp } from 'react-navigation';

export interface Download {
  name: string,
  url: string,
}

export interface Version {
  description?: string | null,
  download: Download,
  version: string,
  publishDate?: string,
}

export interface NavigationScreenProps {
  navigation: NavigationScreenProp<any, any>
}
