import { useFonts } from 'expo-font';
import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator, ScrollView, StyleSheet, Text, View
} from 'react-native';
import { withNavigation } from 'react-navigation';
import fonts from '../../assets/fonts';
import Card from '../../components/Card';
import CustomText from '../../components/CustomText';
import Modal from '../../components/Modal';
import * as Colors from '../../constants';
import refresh from './service';
import { NavigationScreenProps, Version } from './types';

function HomeScreen({ navigation }: NavigationScreenProps) {
  const [loading, setLoading] = useState<boolean>(true);
  const [protonMailVersion, setProtonMailVersion] = useState<Version | null>(null);
  const [signalVersion, setSignalVersion] = useState<Version | null>(null);
  const [firefoxVersion, setFirefoxVersion] = useState<Version | null>(null);
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  const [fontsLoaded] = useFonts(fonts);

  const spinner = (
    <View style={[styles.centeredView, styles.horizontalView]}>
      <ActivityIndicator size="large" color={Colors.mintBlue} />
      <Text style={styles.description}>Let Me Know</Text>
    </View>
  );

  useEffect(() => {
    (async () => {
      const versions = await refresh();

      setProtonMailVersion(versions.protonMail);
      setSignalVersion(versions.signal);
      setFirefoxVersion(versions.firefox);
      setTimeout(() => {
        setLoading(false);
      }, 1000);
    })();
  }, []);

  if (loading || !fontsLoaded) {
    return (
      <View style={styles.centeredView}>
        {spinner}
        <StatusBar backgroundColor={Colors.darkGray} />
      </View>
    );
  }

  if (!protonMailVersion || !signalVersion || !firefoxVersion) {
    return (
      <View style={styles.centeredView}>
        <CustomText>We could not retrieve data at the moment.</CustomText>
        <StatusBar backgroundColor={Colors.darkGray} />
      </View>
    );
  }

  function closeModal() {
    setModalVisible(false);
  }

  function openModal() {
    setModalVisible(true);
  }

  return (
    <ScrollView contentContainerStyle={styles.contentContainer}>
      <StatusBar backgroundColor={Colors.darkGray} />
      {protonMailVersion.description && (
        <Modal
          modalText={protonMailVersion.description}
          modalVisible={modalVisible}
          onClose={closeModal}
        />
      )}
      <View style={styles.cardsWrapper}>
        <Card
          title="Firefox"
          color={Colors.orange}
          appVersion={firefoxVersion}
        />
        <Card
          title="Signal"
          color={Colors.blue}
          appVersion={signalVersion}
        />
        <Card
          title="ProtonMail"
          color={Colors.mintBlue}
          appVersion={protonMailVersion}
          onShowDescription={openModal}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  cardsWrapper: {
    flex: 1,
    justifyContent: 'space-evenly',
    margin: 20,
  },
  centeredView: {
    alignItems: 'center',
    backgroundColor: Colors.black,
    flex: 1,
    justifyContent: 'center',
  },
  contentContainer: {
    backgroundColor: Colors.black,
    flex: 1,
    justifyContent: 'space-evenly',
    padding: 0,
  },
  description: {
    color: Colors.mintBlue,
    fontSize: 20,
    paddingVertical: 10,
  },
  horizontalView: {
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 10,
  },
});

HomeScreen.navigationOptions = {
  headerShown: false,
  headerMode: 'none',
};

export default withNavigation(HomeScreen);
