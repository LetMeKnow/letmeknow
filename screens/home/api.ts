import { Download, Version } from './types';

async function sendRequest(url : string) {
  let response;
  try {
    response = await fetch(url);
  } catch (error) {
    throw new Error(error);
  }

  const json = await response.json();
  return json;
}

function getGithubDownloads(assets: unknown[]): Download[] {
  return assets.map((asset: any) => ({
    name: asset.name,
    url: asset.browser_download_url,
  }));
}

async function fetchFromGithub(repo : string) : Promise<any> {
  const json = await sendRequest(`https://api.github.com/repos/${repo}/releases/latest`);

  return json;
}

export async function fetchProtonMail() : Promise<Version> {
  const json = await fetchFromGithub('ProtonMail/proton-mail-android');
  return {
    description: json.body?.replace(/<br>/g, '\n').replace(/[*|[\]\\]/g, ''),
    version: json.tag_name,
    download: getGithubDownloads(json.assets)[0],
    publishDate: json.published_at,
  };
}

export async function fetchFirefox() : Promise<Version> {
  const json = await fetchFromGithub('mozilla-mobile/fenix');
  const download = getGithubDownloads(json.assets).find((d) => d.url.includes('arm'));
  if (!download) {
    throw new Error('Apk is not available.');
  }

  return {
    description: json.body?.replace(/<br>/g, '\n').replace(/[*|[\]\\]/g, ''),
    version: json.tag_name.replace(/v/g, ''),
    // eslint-disable-next-line object-shorthand
    download: download,
    publishDate: json.published_at,
  };
}

export async function fetchSignal() : Promise<Version> {
  const json = await sendRequest('https://updates.signal.org/android/latest.json');
  return {
    version: json.versionName,
    download: {
      name: `Signal-Android-website-prod-universal-release-${json.versionName}.apk`,
      url: json.url,
    },
  };
}
