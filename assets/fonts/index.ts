export default {
  'Inter-Black': require('./Inter-Black.otf'),
  'Inter-Bold': require('./Inter-Bold.otf'),
  'Inter-Light': require('./Inter-Light.otf'),
  'Inter-Medium': require('./Inter-Medium.otf'),
  'Inter-Regular': require('./Inter-Regular.otf'),
  'Inter-Thin': require('./Inter-Thin.otf'),
};
