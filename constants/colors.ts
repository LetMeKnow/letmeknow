export const black = '#15191e';
export const blue = '#2196F3';
export const darkGray = '#5c6470';
export const veryDarkGray = '#3d3d3d';
export const ivory = '#F7F0D2';
export const mintBlue = '#48ABA9';
export const orange = '#ff8c00';
export const transparentBlack = 'rgba(0, 0, 0, 0.5)';
