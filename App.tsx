import { registerTaskAsync, Result } from 'expo-background-fetch';
import { setNotificationHandler } from 'expo-notifications';
import { defineTask } from 'expo-task-manager';
import React, { ReactElement, useEffect } from 'react';
import Navigator from './screens/home/HomeStack';
import refresh from './screens/home/service';

const BACKGROUND_FETCH_TASK = 'background-fetch';

defineTask(BACKGROUND_FETCH_TASK, async () => {
  refresh();
  return Result.NewData;
});

setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

export default function App(): ReactElement {
  function registerBackgroundFetch() {
    registerTaskAsync(BACKGROUND_FETCH_TASK, {
      minimumInterval: 30 * 60,
      stopOnTerminate: false,
      startOnBoot: true,
    });
  }

  useEffect(() => {
    registerBackgroundFetch();
  }, []);

  return (
    <Navigator />
  );
}
