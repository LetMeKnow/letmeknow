import * as React from 'react';
import { StyleSheet, Text } from 'react-native';
import * as Colors from '../constants';

type StylingProps = {
  fontStyle?: string,
  children: React.ReactNode,
  style?: Record<string, number | string>,
}

export default function CustomText({ fontStyle, children, style }: StylingProps): JSX.Element {
  let font;
  switch (fontStyle) {
  case 'primary':
    font = styles.primaryText;
    break;
  case 'highlight':
    font = styles.highlightText;
    break;
  default:
    font = styles.regularText;
  }
  return (
    <Text style={{ ...font, ...style }}>
      {children}
    </Text>
  );
}

const styles = StyleSheet.create({
  highlightText: {
    color: Colors.ivory,
    fontFamily: 'Inter-Bold',
    fontSize: 14,
  },
  primaryText: {
    fontFamily: 'Inter-Black',
    fontSize: 38,
  },
  regularText: {
    color: Colors.ivory,
    fontFamily: 'Inter-Light',
    fontSize: 14,
  },
});
