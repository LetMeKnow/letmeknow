import { Feather } from '@expo/vector-icons';
import * as React from 'react';
import { StyleSheet } from 'react-native';
import * as Colors from '../constants';

type Props = {
  handleOnPress: () => void;
}

export default function CustomButton({ handleOnPress }: Props): JSX.Element {
  return (
    <Feather.Button
      onPress={handleOnPress}
      name="info"
      iconStyle={{ marginRight: 4 }}
      size={12} // icon size
      style={styles.container}
      color={Colors.ivory}
    >
     { `What's new`}
    </Feather.Button>

  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.mintBlue,
    borderRadius: 4,
    fontSize: 12,
    paddingHorizontal: 6,
    paddingVertical: 0,
  },
});
