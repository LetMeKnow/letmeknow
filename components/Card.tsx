import React from 'react';
import { StyleSheet, View } from 'react-native';
import * as Colors from '../constants';
import { Version } from '../screens/home/types';
import CustomButton from './CustomButton';
import CustomText from './CustomText';
import IconButton from './IconButton';

type CardProps = {
  title: string;
  color: string;
  appVersion: Version;
  onShowDescription?: () => void;
};

export default function Card({
  title,
  color,
  appVersion,
  onShowDescription,
}: CardProps): JSX.Element {
  const { publishDate } = appVersion;

  return (
    <View>
      <View style={styles.primaryWrapper}>
        <CustomText fontStyle="primary" style={{ ...styles.heading, color: color }}>
          {title}
        </CustomText>
        <IconButton download={appVersion.download} iconColor={color} />
      </View>
      <View style={styles.infoWrapper}>
        <View style={styles.horizontalView}>
          <CustomText style={styles.padding}>
            Version:
            {' '}
            {appVersion.version}
          </CustomText>
          {onShowDescription && <CustomButton handleOnPress={onShowDescription} />}
        </View>
        {publishDate && (
          <CustomText style={styles.padding}>
            {`Published: ${publishDate.slice(0, 10)}`}
          </CustomText>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  heading: {
    justifyContent: 'flex-start',
    marginVertical: 12,
  },
  horizontalView: {
    flexDirection: 'row',
  },
  infoWrapper: {
    backgroundColor: Colors.veryDarkGray,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    flexDirection: 'column',
    justifyContent: 'space-between',
    padding: 12,
  },
  padding: {
    paddingRight: 6,
  },
  primaryWrapper: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 12,
  },
});
