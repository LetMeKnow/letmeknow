import React, { ReactElement } from 'react';
import {
  Modal, Pressable, StyleSheet, View,
} from 'react-native';
import * as Colors from '../constants';
import CustomText from './CustomText';

type Props = {
    modalText: string,
    modalVisible: boolean,
    onClose: () => void,
  }

function CustomModal({ modalText, modalVisible, onClose }: Props):ReactElement {
  return (
    <View>
      <Modal
        animationType="slide"
        transparent
        visible={modalVisible}
        onRequestClose={onClose} // Android required
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <CustomText style={styles.description}>{modalText}</CustomText>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={onClose}
            >
              <CustomText fontStyle="highlight">Close</CustomText>
            </Pressable>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 4,
    elevation: 2,
    padding: 6,
  },
  buttonClose: {
    backgroundColor: Colors.mintBlue,
  },
  centeredView: {
    backgroundColor: Colors.transparentBlack,
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
  },
  description: {
    marginBottom: 15,
    textAlign: 'left',
  },
  modalView: {
    alignItems: 'center',
    backgroundColor: Colors.veryDarkGray,
    borderRadius: 4,
    elevation: 8,
    margin: 20,
    padding: 20,
    shadowColor: Colors.ivory,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
});

export default CustomModal;
