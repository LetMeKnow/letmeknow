import { Feather } from '@expo/vector-icons';
import * as Linking from 'expo-linking';
import * as React from 'react';
import { TouchableOpacity } from 'react-native';

type Props = {
  download: {
    name: string,
    url: string,
  },
  iconColor: string,
}

export default function IconButton({ download, iconColor }: Props): JSX.Element {
  return (
    <TouchableOpacity
      onPress={() => Linking.openURL(download.url)}
    >
      <Feather name="download" size={24} color={iconColor} />
    </TouchableOpacity>
  );
}
